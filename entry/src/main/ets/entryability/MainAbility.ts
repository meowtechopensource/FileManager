/*
 * Copyright (c) 2021-2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import UIAbility from '@ohos.app.ability.UIAbility'
import window from '@ohos.window'
import AbilityCommonUtil from '../base/utils/AbilityCommonUtil'
import Logger from '../base/log/Logger'
import { FileUtil } from '../base/utils/FileUtil'

const TAG = 'MainAbility'

export default class MainAbility extends UIAbility {
  onCreate(want, launchParam) {
    Logger.i(TAG, 'onCreate')
    globalThis.abilityContext = this.context
  }

  onDestroy() {
    Logger.i(TAG, 'onDestroy')
    AbilityCommonUtil.releaseMediaLibrary()
  }

  onWindowStageCreate(windowStage: window.WindowStage) {
    // Main window is created, set main page for this ability
    Logger.i(TAG, 'onWindowStageCreate')
    AbilityCommonUtil.init().then(() => {
      windowStage.getMainWindow((err, data) => {
        globalThis.windowClass = data
      })
      windowStage.loadContent('pages/Home', (err, data) => {
        if (err.code) {
          Logger.e(TAG, 'Failed to load the content: ' + JSON.stringify(err));
          return
        }
        Logger.i(TAG, 'data: ' + JSON.stringify(data));
      })
    })

  }

  onWindowStageDestroy() {
    // Main window is destroyed, release UI related resources
    Logger.i(TAG, 'onWindowStageDestroy')
  }

  onForeground() {
    // Ability has brought to foreground
    Logger.i(TAG, 'onForeground')
  }

  onBackground() {
    // Ability has back to background
    Logger.i(TAG, 'onBackground')
  }
}
