# FileManager
<img src="entry/src/main/resources/base/media/app_icon.png" width="128px" />

## 项目介绍
FileManager是 OpenHarmony 的 FilePicker 的一个分支，具有重命名和删除功能。

建议您与<a href="https://gitee.com/ohos-dev/FilePreviewer">FilePreviewer</a>一起使用

## 开发环境
- DevEco Studio 4.0 Release
- SDK API10 4.0.10.13 Release (FULL SDK)

## 截图预览
<img src="figures/Screenshot_65351283871.jpg" width="300px" />
<img src="figures/Screenshot_65351294909.jpg" width="300px" />
<img src="figures/Screenshot_65351302033.jpg" width="300px" />

## 变更日志
### 10.31
- 添加了对 OpenHarmony 4.0 的支持